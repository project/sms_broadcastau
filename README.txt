sms_broadcastau
~~~~~~~~~~

sms_broadcastau is a drupal module to add the smsbroadcast.com.au gateway to
the sms framework module.

INSTALL
~~~~~~~

1. See the getting started guild on installing drupal modules:
http://drupal.org/getting-started/install-contrib/modules

3. Go to admin/smsframework/gateways to setup the SMS BroadcastAU gateway. 

LICENSE
~~~~~~~

This software licensed under the GNU General Public License 2.0.
